package com.volio.baitap10;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BroatcastSms extends BroadcastReceiver {
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        boolean isVersionM =
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
        String format = bundle.getString("format");
//        if(bundle!=null){
//            Object[] pdus=(Object[])bundle.get("pdus");
//            String senderNumber=null;
//            for (int i = 0; i <pdus.length ; i++) {
//                SmsMessage sms=SmsMessage.createFromPdu((byte[])pdus[i]);
//                senderNumber=sms.getOriginatingAddress();
//                String message=sms.getDisplayMessageBody();
//                Toast.makeText(context,"From"+senderNumber+" Message:"+message,Toast.LENGTH_LONG).show();
//
//
//            }
//            SmsManager smsManager=SmsManager.getDefault();
//            smsManager.sendTextMessage(senderNumber,null,"Sorry im kind of busy right now, I call you later",null,null);
//        }
        if (intent.getAction().equals(SMS_RECEIVED)) {
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                if (pdus.length == 0) {
                    return;

                }
                SmsMessage[] messages = new SmsMessage[pdus.length];
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < pdus.length; i++) {
                    if (isVersionM) {
                        // If Android version M or newer:
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                    } else {
                        // If Android version L or older:
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    }
                    stringBuilder.append(messages[i].getDisplayMessageBody());
                }
                String sender = messages[0].getOriginatingAddress();
                long time=messages[0].getTimestampMillis();
                Date date=new Date();
                date.setTime(time);
                String message = stringBuilder.toString();
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                Intent local = new Intent();
                local.setAction("service.to.activity.transfer");
                local.putExtra("txt", message);
                local.putExtra("number", sender);
                local.putExtra("time", date.toString());
                context.sendBroadcast(local);
            }
        }
    }
}
