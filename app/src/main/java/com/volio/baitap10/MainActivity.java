package com.volio.baitap10;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_READ_SMS = 1;
    private static final int REQUEST_READ_PHONE_STATE = 2;
    private static final int REQUEST_RECEIVE_SMS = 3;
     RecyclerView recyclerView;
    RecycleViewSms recycleViewSms;
    BroatcastSms broatcastSms;
    List<Sms> arrayList;
    String txt;
    String time;
    String number;
    EditText editText1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView =findViewById(R.id.rv_sms);
        editText1=findViewById(R.id.am_edit1);
         arrayList=new ArrayList<>();
         recycleViewSms=new RecycleViewSms(arrayList);
        recyclerView.setAdapter(recycleViewSms);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getPermissionToToast();
        getPermissionToReadSMSinbox();
        getPermissionToReceiveSMS();
        IntentFilter filter = new IntentFilter();
        filter.addAction("service.to.activity.transfer");

        broatcastSms=new BroatcastSms(){
            @Override
            public void onReceive(Context context, final  Intent intent) {
                if(intent!=null)
                {
                     txt=intent.getStringExtra("txt");
                     number=intent.getStringExtra("number");
                     time=intent.getStringExtra("time");
                     String justNumber=editText1.getText().toString();
                     if(justNumber.equals(number)) {

                         Sms sms=new Sms(txt,time,number);
                       // textView.setText(sms);
                        recycleViewSms.addSms(sms);
                        recycleViewSms.notifyDataSetChanged();
                     }

                }









































            }
        };
        registerReceiver(broatcastSms, filter);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionToReadSMSinbox() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_SMS)) {
                    // Show our own UI to explain to the user why we need to read the contacts
                    // before actually requesting the permission and showing the default UI
                }
            }

            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.READ_SMS},REQUEST_READ_SMS);
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionToReceiveSMS() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.RECEIVE_SMS)) {
                    // Show our own UI to explain to the user why we need to read the contacts
                    // before actually requesting the permission and showing the default UI
                }
            }

            requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS},REQUEST_RECEIVE_SMS);
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionToToast() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)) {
                    // Show our own UI to explain to the user why we need to read the contacts
                    // before actually requesting the permission and showing the default UI
                }
            }

            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},REQUEST_READ_PHONE_STATE);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == REQUEST_READ_SMS) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "READ_SMS permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "READ_SMS permission denied", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


        if(requestCode == REQUEST_READ_PHONE_STATE){
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "READ_PHONE_STATE permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "READ_PHONE_STATE permission denied", Toast.LENGTH_SHORT).show();
            }

        }else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


        if(requestCode == REQUEST_RECEIVE_SMS){
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "RECEIVE_SMS permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "RECEIVE_SMS permission denied", Toast.LENGTH_SHORT).show();
            }

        }else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
