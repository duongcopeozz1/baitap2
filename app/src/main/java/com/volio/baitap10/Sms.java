package com.volio.baitap10;

import java.util.ArrayList;
import java.util.List;

public class Sms  {
    private String txt;
    private String time;
    private String number;

    public Sms(String txt, String time, String number) {
        this.txt = txt;
        this.time = time;
        this.number = number;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
