package com.volio.baitap10;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class RecycleViewSms extends RecyclerView.Adapter<RecycleViewSms.ViewHolder> {
    public List<Sms> arrayList;
    public interface ABC{
        public void abc();
    }
    ABC a;
    public RecycleViewSms(List<Sms> arrayList) {
        this.arrayList = arrayList;

    }
    public void addSms(Sms sms){
        arrayList.add(sms);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sms,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TextView time=holder.time;
        TextView txt=holder.txt;
        TextView number=holder.number;
        time.setText(arrayList.get(position).getTime());
        txt.setText(arrayList.get(position).getTxt());
        number.setText(arrayList.get(position).getNumber());
    }



    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder  extends RecyclerView.ViewHolder{
        TextView txt;
        TextView number;
        TextView time;
        public ViewHolder(View itemView)
        {
             super(itemView);
             txt=itemView.findViewById(R.id.item_sms_txt);
             time=itemView.findViewById(R.id.item_sms_time);
             number=itemView.findViewById(R.id.item_sms_number);
        }
    }
}
